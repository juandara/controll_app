/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package housemanager3;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author USER
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button boton1, boton2, botonSolo;
    
        
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println(event.getTarget());
        System.out.println(event.getTarget().toString());
        System.out.println(event.getSource().toString());
        
        if(event.getTarget().equals(boton1)){
            System.out.println("You clicked boton 1!");
        }else if(event.getTarget().equals(boton2)){
            System.out.println("You clicked boton 2!");
        }
        /*
        int a=1;
        switch(){
            case boton1.toString(): System.out.println("You clicked boton 1!");
                break;
            default:
                break;
            
        }*/
        
    }
    
    @FXML
    private void handleButtonAction2(ActionEvent event) {
        
        System.out.println("You clicked botonSolo");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        boton1.setOnAction(this::handleButtonAction);
        boton2.setOnAction(this::handleButtonAction);
        botonSolo.setOnAction(this::handleButtonAction2);
        
        
    }    
    
}

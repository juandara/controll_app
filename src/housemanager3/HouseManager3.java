/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package housemanager3;

import java.io.IOException;
import java.util.HashSet;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author USER
 */
public class HouseManager3 extends Application {
    
    private Stage primaryStage;
    
    @Override
    public void start(Stage primaryStage) throws Exception {//este es como un windows started
        
        
        this.primaryStage = primaryStage;
        primaryStage.setTitle("House Manager 1.3");
        //primaryStage.initStyle(StageStyle.TRANSPARENT);//de alguna manera llamo un atribto de transparencia sacado de los css (le quito el borde del jFrame)
        primaryStage.show();//mostramos el epacio del programa
        
        mostrarPantallaPrincipal();
        
    }
    private void mostrarPantallaPrincipal() throws IOException{
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml")); //la clase parent es la ecargada de manejar todo lo del xml
        Scene scene = new Scene(root);//la ecena es la que contiene toda la informacion grafica del XML
        
        scene.getStylesheets().add(getClass().getResource("/css/aplicacion1.css").toExternalForm());
        
        scene.setFill(Color.TRANSPARENT);//hacemos el fill de la escena (lo gris claro del xml) transparente (
        //scene.getStylesheets().add("path/stylesheet.css");//con esto utilizo un archivo css para definir las propieddes
        
        this.primaryStage.setScene(scene); //le asociamos una ecena al espacio del programa (como el JFrame)

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
